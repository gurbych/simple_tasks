class isPowerOf2 {
	public static void main(String[] args) {
		System.out.println(isPowerOf2(Integer.parseInt(args[0])));
	}

	static boolean isPowerOf2(int x) {
		// if(x < 1) {
		// 	return false;
		// }
		// if(x == 1) {
		// 	return true;
		// }
		// if(x % 2 != 0) {
		// 	return false;
		// }
		// return isPowerOf2(x / 2);
		return x > 0 && ( x & (x - 1)) == 0;
	}
}

// byte -> Byte
// boolean -> Boolean
// char -> Character
// short -> Short
// int -> Integer
// long -> Long
// float -> Float
// double -> Double